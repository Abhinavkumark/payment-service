package com.trimindtech.Paymenttermsservice.service.impl;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.trimindtech.Paymenttermsservice.Entity.PaymentServiceEntity;
import com.trimindtech.Paymenttermsservice.helpers.CommonHelper;
import com.trimindtech.Paymenttermsservice.repository.PaymentServiceRepository;
import com.trimindtech.Paymenttermsservice.service.PaymentService;
import com.trimindtech.Paymenttermsservice.util.PaymentServiceUtil;

@Service
public class PaymentServiceImpl implements PaymentService {

	 private static final Logger logger = LoggerFactory.getLogger(PaymentServiceImpl.class);

	@Autowired
	private PaymentServiceRepository paymentServiceRepository;

	@Autowired
	private PaymentServiceUtil paymentServiceUtil;

	@Override
	@Transactional
	public Object savePaymentService(Object saveRequestObject) {
		logger.info("save payment");
		try {
			ObjectMapper mapper = CommonHelper.getMapper();
			PaymentServiceEntity requestPaymentService = mapper.convertValue(saveRequestObject,
					PaymentServiceEntity.class);
			requestPaymentService = paymentServiceUtil.prepareSavePaymentServiceConstants(requestPaymentService);
			paymentServiceRepository.saveAndFlush(requestPaymentService);
			return requestPaymentService;
		} catch (Exception exc) {
			return exc.getMessage();
		}
	}

	@Override
	public Object readAllPaymentServices() {
		logger.info("read all payments");
		return paymentServiceRepository.findAll();
	}

	@Override
	public Object deletePaymentService(Object saveRequestObject) {
		logger.info("delete payment");
		try {
			ObjectMapper mapper = CommonHelper.getMapper();
			PaymentServiceEntity requestPaymentService = mapper.convertValue(saveRequestObject,
					PaymentServiceEntity.class);
			paymentServiceRepository.deleteById(requestPaymentService.getId());
		} catch (Exception exc) {
			return exc.getMessage();
		}
		return "Successfully deleted";
	}

	@Override
	public Object updatePaymentService(Object saveRequestObject) {
		logger.info("update payment");
		try {
			ObjectMapper mapper = CommonHelper.getMapper();
			PaymentServiceEntity requestPaymentService = mapper.convertValue(saveRequestObject,
					PaymentServiceEntity.class);
			if (paymentServiceRepository.existsById(requestPaymentService.getId())) {
				requestPaymentService = paymentServiceUtil.prepareUpdatePaymentServiceConstants(requestPaymentService);
				paymentServiceRepository.saveAndFlush(requestPaymentService);
				return requestPaymentService;
			} else
				return "Entity not found";

		} catch (Exception exc) {
			return exc.getMessage();
		}
	}

}
