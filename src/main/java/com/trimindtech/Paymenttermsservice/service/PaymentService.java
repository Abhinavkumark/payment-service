package com.trimindtech.Paymenttermsservice.service;

public interface PaymentService {

	Object savePaymentService(Object saveRequestObject);

	Object readAllPaymentServices();

	Object deletePaymentService(Object saveRequestObject);

	Object updatePaymentService(Object saveRequestObject);

}
