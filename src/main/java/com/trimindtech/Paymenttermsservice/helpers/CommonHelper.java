package com.trimindtech.Paymenttermsservice.helpers;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class CommonHelper {

	private static ObjectMapper mapper;
	
	public static ObjectMapper getMapper() {
		if(mapper == null)
			mapper =  new ObjectMapper();
		
		return mapper;
	}
}
