package com.trimindtech.Paymenttermsservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.trimindtech.Paymenttermsservice.service.PaymentService;

@RestController
@RequestMapping("paymentservice")
public class PaymentServiceController {

	@Autowired
	private PaymentService paymentService;

	@RequestMapping(value = "savepaymentservice", method = RequestMethod.POST, produces = "application/json")
	public Object savePaymentService(@RequestBody Object saveRequestObject) {
		return paymentService.savePaymentService(saveRequestObject);
	}
	
	@RequestMapping(value = "readpaymentservice", method = RequestMethod.GET, produces = "application/json")
	public Object readAllPaymentServices() {
		return paymentService.readAllPaymentServices();
	}

	@RequestMapping(value = "deletebyid", method = RequestMethod.POST, produces = "application/json")
	public Object deletePaymentService(@RequestBody Object saveRequestObject) {
		return paymentService.deletePaymentService(saveRequestObject);
	}
	
	@RequestMapping(value = "updatepaymentservice", method = RequestMethod.POST, produces = "application/json")
	public Object updatePaymentService(@RequestBody Object saveRequestObject) {
		return paymentService.updatePaymentService(saveRequestObject);
	}
}
