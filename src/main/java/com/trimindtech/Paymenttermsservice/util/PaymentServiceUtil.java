package com.trimindtech.Paymenttermsservice.util;

import java.util.Date;

import org.springframework.stereotype.Component;

import com.trimindtech.Paymenttermsservice.Entity.PaymentServiceEntity;

@Component
public class PaymentServiceUtil {

	public PaymentServiceEntity prepareSavePaymentServiceConstants(PaymentServiceEntity requestPaymentService) {
		requestPaymentService.setCreationDate(new Date());
		requestPaymentService.setDescription("Within "+requestPaymentService.getDays()+" days");
		requestPaymentService.setCode("NET "+requestPaymentService.getDays());
		return requestPaymentService;
	}
	
	public PaymentServiceEntity prepareUpdatePaymentServiceConstants(PaymentServiceEntity requestPaymentService) {
		requestPaymentService.setDescription("Within "+requestPaymentService.getDays()+" days");
		requestPaymentService.setCode("NET "+requestPaymentService.getDays());
		return requestPaymentService;
	}

}
