package com.trimindtech.Paymenttermsservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.trimindtech.Paymenttermsservice.Entity.PaymentServiceEntity;

@Repository
public interface PaymentServiceRepository extends JpaRepository<PaymentServiceEntity, Integer> {

}
