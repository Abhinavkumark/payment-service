package com.trimindtech.Paymenttermsservice.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Setter
@Getter
public class PaymentServiceEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "CreationDate")
	private Date creationDate;
	
	@Column(name = "Code" , unique = true)
	private String code;
	
	@Column(name = "Description")
	private String description;
	
	@Column(name = "Days")
	private Integer days;
	
	@Column(name = "ReminderBeforeDays")
	private Integer reminderBeforeDays;
	
	
}
